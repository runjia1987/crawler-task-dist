create table `biz_type_data` (
	`bizType` varchar(50) not null,
	`script` text not null,
	`childBizType` varchar(50),
	`deleted` tinyint not null default 0,
	`creation_time` timestamp NOT NULL DEFAULT now(),
	`update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
create unique index i_biz_type_data_uniq on biz_type_data(`bizType`);

create table `biz_task` (
	`taskId` varchar(20) not null default '',
	`parentTaskId` varchar(20) not null default '',
	`bizType` varchar(50) not null,
	`tasksCount` int not null default 0,
	`finished` tinyint not null default 0,
	`creation_time` timestamp NOT NULL DEFAULT now()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
create unique index i_biz_task_uniq on biz_task(`taskId`);

alter table biz_task add `arg_str` varchar(1000) not null;