package org.jackJew.biz.task.dist.service.test;

import org.junit.Test;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

public class JsonParserTest {
	
	@Test
	public void testParse() {
		String result = "{rs : [], status: [] }";
		JsonParser jsonParser = new JsonParser();
		JsonElement element = jsonParser.parse(result);
		
		JsonArray array = element.getAsJsonObject().get("rs").getAsJsonArray();
		System.out.println("array size: " + array.size());
	}

}
