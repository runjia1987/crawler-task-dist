package org.jackJew.biz.task.dist.controller.test;

import org.jackJew.biz.task.dist.controller.BizTypeDataController;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/spring/applicationContext.xml"})
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = true)
@Transactional
public class BizTypeDataControllerTest {
	
	@Autowired
	private BizTypeDataController bizTypeDataController;
	
	@Test
	public void testGetByBizType() {
		String bizType = "jd_price";
		String script = bizTypeDataController.getByBizType(bizType);
		System.out.println(script);
	}

}
