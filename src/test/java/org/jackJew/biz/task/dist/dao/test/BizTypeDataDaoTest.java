package org.jackJew.biz.task.dist.dao.test;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.jackJew.biz.task.Constants;
import org.jackJew.biz.task.dist.dao.BizTypeDataDao;
import org.jackJew.biz.task.dist.model.BizTypeData;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ClassUtils;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/applicationContext.xml"})
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = true)
@Transactional
public class BizTypeDataDaoTest {
	
	@Autowired
	private BizTypeDataDao bizTypeDataDao;
	
	@Test
	public void testAdd() {
		ResourcePatternResolver patternResolver = new PathMatchingResourcePatternResolver(ClassUtils.getDefaultClassLoader());
		try {
			Resource[] resources = patternResolver.getResources("classpath:biz/templates/**/*.js");
			for(Resource resource : resources) {
				String name = resource.getFilename();
				String bizType = name.substring(0, name.lastIndexOf("."));
				System.out.println(bizType);
				
				InputStream ins = resource.getInputStream();
				String script = IOUtils.toString(ins, Constants.CHARSET);
				IOUtils.closeQuietly(ins);
				
				BizTypeData bizTypeData = new BizTypeData(bizType, script);
				bizTypeDataDao.add(bizTypeData);
			}
		} catch (IOException e) {
		}		
	}

}
