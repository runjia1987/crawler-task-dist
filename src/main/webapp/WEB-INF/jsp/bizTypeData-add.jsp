<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
	String basePath = request.getScheme() + "://" + request.getServerName()
			+ ":" + request.getServerPort()+ request.getContextPath() + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>bizTypeData-add</title>
</head>
<body>
<h3>bizTypeData-add: </h3><br/>
<form method="post" action="<%=basePath %>bizTypeData/upload" enctype="multipart/form-data"
		onsubmit="return checkForm()">
<input type="text" name="bizType" size="30" /><br/>
<input type="text" name="childBizType" size="30" /><br/>
<input type="file" name="scriptFile" /><br/>

<br/>
<input type="submit" value="Add" /> &nbsp;
<input type="button" value="Back" onclick="location.href='navigate'" />
</form>
</body>
<script type="text/javascript">
	function checkForm() {
		var bizType = document.getElementsByName("bizType")[0].value;
		var scriptFile = document.getElementsByName("scriptFile")[0].value;
		if(bizType == null || bizType == '') {
			alert("bizType can not be null.");
			return false;
		}
		if(scriptFile == null || scriptFile == '') {
			alert("file can not be null.");
			return false;
		}
		return true;
	}
</script>
</html>