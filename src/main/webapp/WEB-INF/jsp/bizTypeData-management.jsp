<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.util.List, org.jackJew.biz.task.dist.model.BizTypeData" %>
<%@ page import="org.jackJew.biz.task.dist.util.BaseUtils" %>
<%
	String basePath = request.getScheme() + "://" + request.getServerName()
			+ ":" + request.getServerPort() + request.getContextPath() + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>bizTypeData-management</title>
<script type="text/javascript" src="<%=basePath %>resources/scripts/jquery-1.11.3.min.js"></script>
</head>
<body>
<h3>bizTypeData-management: </h3>
<input type="button" value="Add New" onclick="addNew()" /><br/>
<%
	List<BizTypeData> bizTypeDatas = (List<BizTypeData>)request.getAttribute("bizTypeDatas");
	if(BaseUtils.isNullOrEmpty(bizTypeDatas)) {
		out.print("no bizTypeDatas.");
	} else {
		for(BizTypeData bizTypeData : bizTypeDatas) {
			out.print("<div style='color:navy'>bizType: " + bizTypeData.getBizType()
				+ ",childBizType: " + bizTypeData.getChildBizType()
				+ ", deleted: " + bizTypeData.isDeleted()
				+ ", create_time: " + BaseUtils.formatDate(bizTypeData.getCreation_time(), BaseUtils.full_Formatter)
				+ ", update_time: " + BaseUtils.formatDate(bizTypeData.getUpdate_time(), BaseUtils.full_Formatter)
			);
			StringBuilder builder = new StringBuilder(1 << 7);
			builder.append("&nbsp;&nbsp;").append("<input type='button' value='View Script' onclick='view(\"")
					.append(bizTypeData.getBizType())
					.append("\")' /> &nbsp;");
			builder.append("<input type='button' value='Modify' onclick='modify(\"")
					.append(bizTypeData.getBizType())
					.append("\")' /> &nbsp;");
			builder.append("<input type='button' value='Delete' onclick='del(\"")
					.append(bizTypeData.getBizType())
					.append("\")' /></div> \r\n");
			builder.append("<textarea style='width:560px;height:200px;margin-top:10px;margin-bottom:10px;display:none;color:#a97e1f;' id='ta_")
					.append(bizTypeData.getBizType()).append("'></textarea><br/> \r\n");
			
			out.print(builder.toString());
		}
	}
%>
<br/>

<script type="text/javascript">
	function view(bizType) {
		var je = $('#ta_' + bizType);
		if(je.css('display') != 'none') {
			return;
		}
		$.get("<%=basePath %>bizTypeData/getBizScript", {'bizType': bizType}, function(data) {
					je.css('display','block');
					je.text(data);
					je.bind('dblclick', function() {
						$(this).unbind('dblclick');
						$(this).css('display', 'none');  // hide
					});
				});
	}
	
	function addNew() {
		location.href = '<%=basePath %>bizTypeData/addNew';
	}
	
	function del(bizType) {
		$.post('<%=basePath %>bizTypeData/delete', {'bizType': bizType}, function(data) {
			alert(data);
		});
	}
	
	function modify(bizType) {
		location.href = '<%=basePath %>bizTypeData/updateOne?bizType=' + bizType;
	}
</script>
</body>
</html>