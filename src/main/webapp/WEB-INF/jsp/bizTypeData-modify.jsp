<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.util.List, org.jackJew.biz.task.dist.model.BizTypeData" %>
<%
	String basePath = request.getScheme() + "://" + request.getServerName()
			+ ":" + request.getServerPort()+ request.getContextPath() + "/";
	BizTypeData data = (BizTypeData)request.getAttribute("bizTypeData");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>bizTypeData-add</title>
</head>
<body>
<h3>bizTypeData-modify: </h3><br/>
<form method="post" action="<%=basePath %>bizTypeData/modify" enctype="multipart/form-data"
		onsubmit="return checkForm()">
bizType: <input type="text" name="bizType" size="30" value="<%=data.getBizType()%>" />
<br/><br/>
childBizType: <input type="text" name="childBizType" size="30" value="<%=data.getChildBizType()%>" />
<br/><br/>
<div>create_time: <%=data.getCreation_time() %>,
<br/><br/>
update_time: <%=data.getUpdate_time() %></div>
<br/>
<textarea name="scriptContent" rows="40" cols="80"><%=data.getScript() %></textarea>

<br/>
<input type="submit" value="Modify" /> &nbsp;
<input type="button" value="Back" onclick="location.href='navigate'" />
</form>
</body>
<script type="text/javascript">
	function checkForm() {
		var bizType = document.getElementsByName("bizType")[0].value;
		var scriptContent = document.getElementsByName("scriptContent")[0].value;
		if(bizType == null || bizType == '') {
			alert("bizType can not be null.");
			return false;
		}
		if(scriptContent == null || scriptContent == '') {
			alert("scriptContent can not be null.");
			return false;
		}
		return true;
	}
</script>
</html>