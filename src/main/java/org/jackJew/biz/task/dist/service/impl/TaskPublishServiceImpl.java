package org.jackJew.biz.task.dist.service.impl;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.jackJew.biz.task.Constants;
import org.jackJew.biz.task.TaskObject;
import org.jackJew.biz.task.dist.model.BizTask;
import org.jackJew.biz.task.dist.service.TaskPublishService;
import org.jackJew.biz.task.dist.util.BaseUtils;
import org.jackJew.biz.task.dist.util.PropertyReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.MessageProperties;

/**
 * message push service
 * @author Jack
 *
 */
@Service
public class TaskPublishServiceImpl implements TaskPublishService {
	
	private final static Logger logger = LoggerFactory.getLogger(TaskPublishServiceImpl.class);
	
	public final static String mqFactoryUri = PropertyReader.getProperty("mqFactoryUri");
	
	private final static String exchangeName = PropertyReader.getProperty("exchangeName");
	private final static String queueName = PropertyReader.getProperty("queueName");
	
	private final static int channelsPoolSize = Integer.valueOf(PropertyReader.getProperty("channelsPoolSize"));
	
	private final ConnectionFactory connectionFactory = new ConnectionFactory();
	
	private Connection conn;
	
	/**
	 * tasks queue
	 */
	private LinkedBlockingQueue<BizTask> taskQueue = new LinkedBlockingQueue<>();
	
	/**
	 * publish executor
	 */
	private ExecutorService publishExecutorService = Executors.newFixedThreadPool(channelsPoolSize);
	
	@PostConstruct
	private void init() {
		try {
			connectionFactory.setUri(mqFactoryUri);
			connectionFactory.setAutomaticRecoveryEnabled(true);
			connectionFactory.setTopologyRecoveryEnabled(true);
			conn = connectionFactory.newConnection();
			
			// set up
			for(int i = 0; i < channelsPoolSize; i++) {
				publishExecutorService.submit(() -> {
					Channel channel = conn.createChannel();						
					while(true) {
						try {
							TaskObject task = taskQueue.take();
							channel.basicPublish(
									exchangeName,
									queueName,
									MessageProperties.PERSISTENT_BASIC,
									BaseUtils.GSON.toJson(task).getBytes(Constants.CHARSET));
						} catch(Exception e) {
							logger.error("", e);
						}							
					}					
				});
			}
		} catch (Exception e) {
			logger.error("", e);
		}
	}
	
	@PreDestroy
	private void destroy() throws Exception {
		taskQueue.clear();
		publishExecutorService.shutdown();
		publishExecutorService.awaitTermination(5000, TimeUnit.SECONDS);
		if(conn != null) {
			conn.close();			
		}
	}

	@Override
	public void submit(BizTask task) {
		taskQueue.offer(task);
	}
	
	public void submit(List<BizTask> tasks) {
		taskQueue.addAll(tasks);
	}

	public ConnectionFactory getConnectionFactory() {
		return connectionFactory;
	}

}