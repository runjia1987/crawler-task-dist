package org.jackJew.biz.task.dist.util;

import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.Locale;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * utilities
 * @author Jack
 */
public class BaseUtils {
	
	public static final Gson GSON = new GsonBuilder().disableHtmlEscaping().create();
	
	public static final DateTimeFormatter yyyyMMdd_Formatter =
			DateTimeFormatter.ofPattern("yyyy-MM-dd")
			.withLocale(Locale.getDefault())
			.withZone(ZoneId.systemDefault());
	
	public static final DateTimeFormatter full_Formatter =
			DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")
			.withLocale(Locale.getDefault())
			.withZone(ZoneId.systemDefault());
	
	public final static String STRICT_JS_DECLARATION = "\"use strict\";";
	
	public static boolean isNullOrEmpty(Map<?, ?> map) {
		return map == null || map.isEmpty();
	}
	
	public static boolean isNullOrEmpty(Collection<?> collection) {
		return collection == null || collection.isEmpty();
	}

	public static boolean isEmpty(String source) {
		return source == null || source.isEmpty();
	}
	
	public static int getLength(String source) {
		return source == null ? 0 : source.length();
	}
	
	public static String getSimpleExMsg(Exception e) {
		return e == null ? "" : (e.getClass().getName() + " - " + e.getMessage());
	}
	
	public static String formatDate(java.util.Date date, DateTimeFormatter df) {
		return df.format(date.toInstant());
	}
	
}
