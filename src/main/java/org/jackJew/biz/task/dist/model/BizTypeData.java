package org.jackJew.biz.task.dist.model;

import java.util.Date;

public class BizTypeData {
	
	private String bizType; // unique
	private String script;	// related execution script
	private String childBizType; // could be null
	private boolean deleted;  // "deleted" flag
	private java.util.Date creation_time;
	private java.util.Date update_time;
	private String owner;	

	public BizTypeData() {}
	
	public BizTypeData(String bizType, String script) {
		this.bizType = bizType;
		this.script = script;
		this.creation_time = this.update_time = new Date();
	}
	
	public String getBizType() {
		return bizType;
	}
	
	public void setBizType(String bizType) {
		this.bizType = bizType;
	}
	
	public String getScript() {
		return script;
	}
	
	public void setScript(String script) {
		this.script = script;
	}
	
	public String getChildBizType() {
		return childBizType;
	}
	
	public void setChildBizType(String childBizType) {
		this.childBizType = childBizType;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public java.util.Date getCreation_time() {
		return creation_time;
	}

	public void setCreation_time(java.util.Date creation_time) {
		this.creation_time = creation_time;
	}

	public java.util.Date getUpdate_time() {
		return update_time;
	}

	public void setUpdate_time(java.util.Date update_time) {
		this.update_time = update_time;
	}
	
	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

}
