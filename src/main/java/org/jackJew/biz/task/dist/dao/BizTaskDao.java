package org.jackJew.biz.task.dist.dao;

import java.util.List;
import java.util.Map;

import org.jackJew.biz.task.dist.model.BizTask;

public interface BizTaskDao {
	
	List<BizTask> find(Map<String, Object> params);
	
	void insert(BizTask bizTask);
	
	void batchInsert(List<BizTask> bizTasks);
	
	BizTask get(String taskId);
	
	/**
	 * 
	 * @param params requires tasksCount, finished, taskId
	 */
	boolean update(Map<String, Object> params);

}