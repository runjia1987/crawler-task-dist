package org.jackJew.biz.task.dist.service;

import org.jackJew.biz.task.BizScript;

/**
 * publish script changes
 * @author Jack
 *
 */
public interface ScriptPublishService {
	
	/**
	 * publish bizScript message
	 */
	void publish(BizScript bizScript);

}
