package org.jackJew.biz.task.dist.util;

import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.LoggerFactory;

/**
 * taskId creator
 * @author Jack
 *
 */
public class TaskIdCreator {
	
	private final static String HOST_RAND_ID;
	
	private final static AtomicInteger INCR = new AtomicInteger(1);
	private final static int MAX_NUM_PER_SECOND = 10000;
	
	static {
		String randomId = ThreadLocalRandom.current().nextInt(1000) + "";
		if(randomId.length() == 1) {
			randomId = "00" + randomId;
		} else if(randomId.length() == 2) {
			randomId = "0" + randomId;
		}
		HOST_RAND_ID = randomId;
		LoggerFactory.getLogger(TaskIdCreator.class).info("HOST_RAND_ID: " + HOST_RAND_ID);
	}
	
	/**
	 * create new task id
	 * @return
	 */
	public static String create() {
		boolean done = false;
		while(!done) {
			int current = INCR.get();
			if(current >= MAX_NUM_PER_SECOND) {
				done = INCR.compareAndSet(current, 1);
				if(done) {
					if((current = INCR.getAndIncrement()) < MAX_NUM_PER_SECOND) {
						return System.currentTimeMillis()/1000 + HOST_RAND_ID + current;
					} else {
						done = false;
					}
				}
			} else {
				if((current = INCR.getAndIncrement()) < MAX_NUM_PER_SECOND) {
					return System.currentTimeMillis()/1000 + HOST_RAND_ID + current;
				}
			}
		}
		throw new IllegalStateException("illegal state when create task id.");
	}

}
