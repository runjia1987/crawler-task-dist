package org.jackJew.biz.task.dist.service;

import org.jackJew.biz.task.Reply;

/**
 * task progress notify service.
 * @author Jack
 *
 */
public interface TaskProgressNotifyService {
	
	/**
	 * notify task status on reply
	 * @param reply
	 */
	void notify(Reply reply);

}
