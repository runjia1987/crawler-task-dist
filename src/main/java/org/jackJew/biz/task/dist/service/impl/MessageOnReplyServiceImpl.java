package org.jackJew.biz.task.dist.service.impl;

import java.io.IOException;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.jackJew.biz.task.Constants;
import org.jackJew.biz.task.Reply;
import org.jackJew.biz.task.dist.service.TaskPublishService;
import org.jackJew.biz.task.dist.service.TaskProgressNotifyService;
import org.jackJew.biz.task.dist.util.BaseUtils;
import org.jackJew.biz.task.dist.util.PropertyReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

@Service
public class MessageOnReplyServiceImpl {
	
	private final static Logger logger = LoggerFactory.getLogger(MessageOnReplyServiceImpl.class);
	
	private final static String queueNameReply = PropertyReader.getProperty("queueNameReply");
	
	public final static int threadPoolSize = Integer.valueOf(PropertyReader.getProperty("threadPoolSize"));
	
	private Channel channel;
	
	private final ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(
			threadPoolSize,
			threadPoolSize,
			5, TimeUnit.MINUTES,
			new LinkedBlockingQueue<Runnable>());
	
	@Autowired
	private TaskPublishService taskPublishService;
	
	@Autowired
	private TaskProgressNotifyService taskProgressNotifyService;

	@PostConstruct
	private void init() {
		try {
			Connection connection = taskPublishService.getConnectionFactory().newConnection(threadPoolExecutor);
			channel = connection.createChannel();
			channel.basicQos(50, false);
			final Consumer consumer = new DefaultConsumer(channel) {
				@Override
				public void handleDelivery(String consumerTag, Envelope envelope,
						BasicProperties properties, byte[] body) throws IOException {
					try {
						Reply reply = BaseUtils.GSON.fromJson(new String(body, Constants.CHARSET), Reply.class);
						// notify
						taskProgressNotifyService.notify(reply);
					} catch(Exception e) {
						logger.error("", e);
					}
				}
				
				@Override
				public void handleCancel(String consumerTag) throws IOException {
					logger.error("consumer on queue " + queueNameReply + " get unexpected cancel signal.");
				}
			};
			channel.basicConsume(queueNameReply, true, consumer);
		} catch (Exception e) {
			logger.error("", e);
		}
	}
	
	@PreDestroy
	private void destroy() throws Exception {
		if(channel != null) {
			Connection conn = channel.getConnection();
			conn.close();
		}
		threadPoolExecutor.shutdown();
	}

}
