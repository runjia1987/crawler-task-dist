package org.jackJew.biz.task.dist.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jackJew.biz.task.Reply;
import org.jackJew.biz.task.dist.dao.BizTaskDao;
import org.jackJew.biz.task.dist.dao.BizTypeDataDao;
import org.jackJew.biz.task.dist.model.BizTask;
import org.jackJew.biz.task.dist.service.TaskProgressNotifyService;
import org.jackJew.biz.task.dist.service.TaskPublishService;
import org.jackJew.biz.task.dist.util.BaseUtils;
import org.jackJew.biz.task.dist.util.TaskIdCreator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

/**
 * task progress notify service implementation.
 * @author Jack
 *
 */
@Service
public class TaskProgressNotifyServiceImpl implements TaskProgressNotifyService {
	
	private final static Logger logger = LoggerFactory.getLogger(TaskProgressNotifyService.class);
	
	@Autowired
	private TaskPublishService taskPublishService;
	
	@Autowired
	private BizTaskDao bizTaskDao;
	@Autowired
	private BizTypeDataDao bizTypeDataDao;
		
	private final JsonParser jsonParser = new JsonParser();

	@Override
	public void notify(Reply reply) {
		final String taskId = reply.getTaskId();
		final String bizType = reply.getBizType();
		String result = reply.getResult();  // expected format: {rs: [], status: []}		
		logger.info("received reply[" + taskId + "] " + "bizType [" + bizType + "], length: " + BaseUtils.getLength(result));
		
		if(!BaseUtils.isEmpty(taskId) && !BaseUtils.isEmpty(bizType) && !BaseUtils.isEmpty(result)) {
			final String childBizType = bizTypeDataDao.getChildBizType(bizType);
			
			JsonElement je = jsonParser.parse(result);				
			JsonArray rsArray = je.getAsJsonObject().getAsJsonArray("rs");
			String status = je.getAsJsonObject().getAsJsonArray("status").get(0).getAsString();			
			boolean isFinished = isTaskCompleted(status);
			
			if(BaseUtils.isEmpty(childBizType)) {
				// no child tasks
				Map<String, Object> updates = new HashMap<>();
				updates.put("taskId", taskId);
				updates.put("finished", isFinished);
				updates.put("tasksCount", 1);
				bizTaskDao.update(updates);
				
				if(isFinished) {
					// TODO save rs
				}
				logger.info("updated task " + taskId + " status to DB.");
			} else {
				// trigger child tasks
				final int size = rsArray.size();
				if(isFinished && size > 0) { // not empty
					List<String> triggeredTaskUrls = new ArrayList<>();
					for(JsonElement e : rsArray) {
						triggeredTaskUrls.add(e.getAsString());
					}
					List<BizTask> bizTasks = new ArrayList<>(size);
					for(String taskUrl : triggeredTaskUrls) {
						String newTaskId = TaskIdCreator.create();
						BizTask bizTask = new BizTask(newTaskId, taskId, childBizType);
						Map<String, String> args = new HashMap<>();
						args.put("url", taskUrl);
						bizTask.setArgs(args);
						bizTasks.add(bizTask);
					}
					triggeredTaskUrls.clear();
					// submit new tasks in async bulk
					taskPublishService.submit(bizTasks);
					
					// batch insert to DB
					bizTaskDao.batchInsert(bizTasks);
					bizTasks.clear();
				}
			}
		}
	}
	
	private boolean isTaskCompleted(String status) {
		return "SUCCESS".equalsIgnoreCase(status) || status.matches("^[0]+$");
	}

}