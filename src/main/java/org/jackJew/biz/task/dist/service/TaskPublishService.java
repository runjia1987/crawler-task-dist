package org.jackJew.biz.task.dist.service;

import java.util.List;

import org.jackJew.biz.task.dist.model.BizTask;

import com.rabbitmq.client.ConnectionFactory;

/**
 * message publish service
 * @author Jack
 *
 */
public interface TaskPublishService {
	
	/**
	 * submit task message
	 */
	void submit(BizTask task);
	
	/**
	 * submit task messages
	 */
	void submit(List<BizTask> tasks);
	
	/**
	 * get shared connectionFactory
	 * @return
	 */
	ConnectionFactory getConnectionFactory();

}
