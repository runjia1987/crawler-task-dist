package org.jackJew.biz.task.dist.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jackJew.biz.task.dist.dao.BizTaskDao;
import org.jackJew.biz.task.dist.gson.dto.SingleResultGson;
import org.jackJew.biz.task.dist.model.BizTask;
import org.jackJew.biz.task.dist.service.TaskPublishService;
import org.jackJew.biz.task.dist.util.GsonUtils;
import org.jackJew.biz.task.dist.util.TaskIdCreator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * manually publish task, typically for test
 * @author Jack
 *
 */
@RestController
@RequestMapping("/task")
public class TaskPublishController {
	
	@Autowired
	private TaskPublishService taskPublishService;
	@Autowired
	private BizTaskDao bizTaskDao;
	
	@RequestMapping(value = "/publish", method = RequestMethod.GET)
	@ResponseBody
	public String publish(@RequestParam String bizType, @RequestParam String url) {
		BizTask bizTask = new BizTask(TaskIdCreator.create(), bizType);
		Map<String, String> args = new HashMap<>(2, 1);
		args.put("url", url);
		bizTask.setArgs(args);
		
		bizTask.postProcessBeforeWrite();
		bizTaskDao.insert(bizTask);
		
		taskPublishService.submit(bizTask);
		return GsonUtils.toJson(new SingleResultGson(SingleResultGson.STATUS.SUCCESS.name()));
	}
	
	@RequestMapping(value = "checkBizTask", method = RequestMethod.GET)
	public String checkBizTask() {
		List<BizTask> tasks = bizTaskDao.find(null);  // find all
		return GsonUtils.toJson(tasks);
	}

}
