package org.jackJew.biz.task.dist.dao;

import java.util.List;

import org.jackJew.biz.task.dist.model.BizTypeData;

public interface BizTypeDataDao {
	
	List<BizTypeData> getAllTypes();
	
	void add(BizTypeData bizTypeData);
	
	void update(BizTypeData bizTypeData);
	
	void delete(String bizType);
	
	BizTypeData getOne(String bizType);

	String getChildBizType(String bizType);
}
