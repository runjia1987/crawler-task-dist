package org.jackJew.biz.task.dist.controller;

import java.io.InputStream;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.jackJew.biz.task.BizScript;
import org.jackJew.biz.task.Constants;
import org.jackJew.biz.task.dist.dao.BizTypeDataDao;
import org.jackJew.biz.task.dist.gson.dto.SingleResultGson;
import org.jackJew.biz.task.dist.model.BaseResponse;
import org.jackJew.biz.task.dist.model.BizTypeData;
import org.jackJew.biz.task.dist.service.ScriptPublishService;
import org.jackJew.biz.task.dist.util.BaseUtils;
import org.jackJew.biz.task.dist.util.GsonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * controller for bizTypeData management
 * @author Jack
 *
 */
@RestController
@RequestMapping("/bizTypeData")
public class BizTypeDataController {
	
	private final static Logger logger = LoggerFactory.getLogger(BizTypeDataController.class);
	
	@Autowired
	private ScriptPublishService scriptPublishService;
	
	@Autowired
	private BizTypeDataDao bizTypeDataDao;
	
	private final Gson gson = new GsonBuilder().create();
	
	@RequestMapping(value = "/getBizScript", method = RequestMethod.GET)
	public String getByBizType(@RequestParam String bizType) {
		BizTypeData bizTypeData = bizTypeDataDao.getOne(bizType);
		if(bizTypeData != null) {
			String script = bizTypeData.getScript();
			return script;
		} else{
			return GsonUtils.toJson(new SingleResultGson("not exists"));
		}
	}	
	
	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	public String upload(@RequestParam String bizType,
				@RequestParam MultipartFile scriptFile, @RequestParam(required = false) String childBizType) {
		try (InputStream ins = scriptFile.getInputStream();) {
			String script = IOUtils.toString(ins, Constants.CHARSET);
			if(!verifyScriptFormat(script)) {
				BaseResponse response = new BaseResponse();
				response.setResult("script must be " + BaseUtils.STRICT_JS_DECLARATION);
				return gson.toJson(response);
			}
			BizTypeData bizTypeData = new BizTypeData(bizType, script);
			bizTypeData.setChildBizType(childBizType);			
			// save to DB.
			bizTypeDataDao.add(bizTypeData);
			
			scriptPublishService.publish(new BizScript(bizType, script, false));
			
			return GsonUtils.toJson(new SingleResultGson(SingleResultGson.STATUS.SUCCESS.name()));
		} catch(Exception e) {
			logger.error("", e);
		}
		return "{result:'FAIL'}";
	}
	
	private boolean verifyScriptFormat(String script) {
		// js Script should use strict mode
		if(!script.startsWith(BaseUtils.STRICT_JS_DECLARATION)) {
			return false;
		}
		return true;
	}
	
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public String delete(@RequestParam String bizType) {
		// update deleted flag
		bizTypeDataDao.delete(bizType);		
		
		// notify crawler clients
		scriptPublishService.publish(new BizScript(bizType, null, true));
		return GsonUtils.toJson(new SingleResultGson(SingleResultGson.STATUS.SUCCESS.name()));
	}
	
	@RequestMapping(value = "/navigate", method = RequestMethod.GET)
	public ModelAndView navigate() {
		ModelAndView mav = new ModelAndView("bizTypeData-management");
		List<BizTypeData> bizTypeDatas = bizTypeDataDao.getAllTypes();
		mav.addObject("bizTypeDatas", bizTypeDatas);
		return mav;
	}
	
	@RequestMapping(value = "/addNew", method = RequestMethod.GET)
	public ModelAndView addNew() {
		return new ModelAndView("bizTypeData-add");
	}
	
	@RequestMapping(value = "/updateOne", method = RequestMethod.GET)
	public ModelAndView updateView(@RequestParam String bizType) {
		BizTypeData bizTypeData = bizTypeDataDao.getOne(bizType);
		ModelAndView mav = new ModelAndView("bizTypeData-modify");
		mav.addObject("bizTypeData", bizTypeData);
		return mav;
	}
	
	@RequestMapping(value = "/modify", method = RequestMethod.POST)
	@ResponseBody
	public String modify(@RequestParam String bizType,
			@RequestParam String scriptContent, @RequestParam(required = false) String childBizType) {
		if(BaseUtils.isEmpty(bizType) || BaseUtils.isEmpty(scriptContent)) {
			return GsonUtils.toJson(new SingleResultGson("bizType and scriptContent should not be null."));
		}
		BizTypeData bizTypeData = new BizTypeData(childBizType, scriptContent);
		bizTypeData.setChildBizType(childBizType);
		bizTypeDataDao.update(bizTypeData);
		
		scriptPublishService.publish(new BizScript(bizType, scriptContent, false));
		
		return GsonUtils.toJson(new SingleResultGson(SingleResultGson.STATUS.SUCCESS.name()));
	}

}
