package org.jackJew.biz.task.dist.model;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.jackJew.biz.task.TaskObject;
import org.jackJew.biz.task.dist.util.BaseUtils;
import org.jackJew.biz.task.dist.util.GsonUtils;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class BizTask extends TaskObject {
	
	private String parentTaskId;
	private int tasksCount;
	private boolean finished;
	
	private Date creation_time;
	private String argstr;
	
	public void postProcessBeforeWrite() {
		if(super.getArgs() != null) {
			argstr = GsonUtils.toJson(super.getArgs());
		}
	}
	
	public void postProcessAfterRead() {
		if(!BaseUtils.isEmpty(argstr)) {
			JsonObject jo = GsonUtils.fromJson(argstr, JsonObject.class);
			Map<String, String> args = new HashMap<>();
			Iterator<Entry<String, JsonElement>> itr = jo.entrySet().iterator();
			while(itr.hasNext()) {
				Entry<String, JsonElement> entry = itr.next();
				args.put(entry.getKey(), entry.getValue().getAsString());
			}
			super.setArgs(args);
		}
	}
	
	public BizTask() {
		super(null);
	}
	
	public BizTask(String taskId) {
		this(taskId, null, null);
	}
	
	public BizTask(String taskId, String bizType) {
		this(taskId, null, bizType);
	}
	
	public BizTask(String taskId, String parentTaskId, String bizType) {
		super(taskId, bizType);
		this.parentTaskId = parentTaskId;
		this.creation_time = new Date();
	}

	public int getTasksCount() {
		return tasksCount;
	}

	public void setTasksCount(int tasksCount) {
		this.tasksCount = tasksCount;
	}

	public String getParentTaskId() {
		return parentTaskId;
	}

	public java.util.Date getCreation_time() {
		return creation_time;
	}

	public boolean isFinished() {
		return finished;
	}

	public void setFinished(boolean finished) {
		this.finished = finished;
	}

	public String getArgstr() {
		return argstr;
	}

}
