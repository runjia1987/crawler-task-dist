package org.jackJew.biz.task.dist.service.impl;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.jackJew.biz.task.BizScript;
import org.jackJew.biz.task.Constants;
import org.jackJew.biz.task.dist.service.ScriptPublishService;
import org.jackJew.biz.task.dist.service.TaskPublishService;
import org.jackJew.biz.task.dist.util.BaseUtils;
import org.jackJew.biz.task.dist.util.PropertyReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.MessageProperties;

@Service
public class ScriptPublishServiceImpl implements ScriptPublishService {
	
	private final static Logger logger = LoggerFactory.getLogger(ScriptPublishServiceImpl.class);
	private final static String script_exchange = PropertyReader.getProperty("script_exchange");
	
	@Autowired
	private TaskPublishService taskPublishService;
	
	private Channel channel;
	
	@PostConstruct
	private void init() {
		try {
			Connection conn = taskPublishService.getConnectionFactory().newConnection();
			channel = conn.createChannel();
			channel.exchangeDeclarePassive(script_exchange);
		} catch (Exception e) {
			logger.error("", e);
		}
	}
	
	@PreDestroy
	private void destroy() throws Exception {
		if(channel != null) {
			Connection conn = channel.getConnection();
			conn.close();
		}
	}

	@Override
	public void publish(BizScript bizScript) {
		try {
			channel.basicPublish(script_exchange, "", MessageProperties.PERSISTENT_BASIC,
					BaseUtils.GSON.toJson(bizScript).getBytes(Constants.CHARSET));
			
			logger.info("has published script of " + bizScript.getBizType());
		} catch (Exception e) {
			logger.error("", e);
		}
	}

}
