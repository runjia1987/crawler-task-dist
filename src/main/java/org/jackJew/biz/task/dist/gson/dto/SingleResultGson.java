package org.jackJew.biz.task.dist.gson.dto;

public class SingleResultGson {
	
	public static enum STATUS {
		SUCCESS,
		FAIL,
		;
	}
	private String status;
	
	public SingleResultGson(String status) {
		this.status = status;
	}

	public String getStatus() {
		return status;
	}

}
